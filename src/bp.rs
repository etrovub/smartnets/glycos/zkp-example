use bulletproofs::r1cs::{
    ConstraintSystem, LinearCombination, Prover, R1CSError, R1CSProof, Variable, Verifier,
};
use bulletproofs::{BulletproofGens, PedersenGens};
use curve25519_dalek::scalar::Scalar;
use merlin::Transcript;

/// Returns 1 if v == 0, 0 otherwise.
///
/// # Arguments
///
/// * cs: ConstraintSystem
/// * v
/// * v_witness: witness of v (Some for Prover, None for Verifier)
///
/// # Details
/// If v != 0: v * v^-1 = 1
/// If v  = 0: v * v^-1 = 0
/// So: 1 - (v * v^-1) gives us the result we want
pub fn is_zero<CS: ConstraintSystem>(
    cs: &mut CS,
    v: impl Into<LinearCombination>,
    v_witness: Option<impl Into<Scalar>>,
) -> Result<LinearCombination, R1CSError> {
    // If witness is zero, inverse element is zero
    let mult = v_witness.map(|witness| {
        let witness = witness.into();
        let inv = if witness == Scalar::zero() {
            Scalar::zero()
        } else {
            witness.invert()
        };
        (witness, inv)
    });
    // Multiply witness and its inv
    let (_v, _inverse, prod) = cs.allocate_multiplier(mult)?;
    // Subtract from 1
    let out = -prod + 1u64;

    // Constrain that it was generated correctly: x * is_zero(x) = 0
    // Otherwise an attacker could choose constraints so that it always returns false.
    let (_, _, in_mul_out) = cs.multiply(v.into(), out.clone());
    cs.constrain(in_mul_out.into());

    Ok(out)
}

/// # CensorProof: Small dalek-bulletproofs example
///
/// Context: Peggy wants to prove that her message does not contain a bad word.
pub struct CensorProof(R1CSProof);
impl CensorProof {
    /// Constrains the ConstraintSystem to prove that the chosen word does not
    /// appear in the supplied text.
    ///
    /// # Arguments
    ///
    /// * cs: ConstraintSystem (Prover or verifier)
    /// * msg: Variables for the message
    /// * msg_witness: Witness of the message (Some for Prover, None for Verifier)
    /// * bad_word: Censored word as u8 slice
    ///
    /// # Details
    /// Since the verifier does not have access to the witness, we have to calculate
    /// the witness data for the negation separately.
    fn gadget<CS: ConstraintSystem>(
        cs: &mut CS,
        msg: Vec<Variable>,
        msg_witness: Option<&[u8]>,
        bad_word: &[u8],
    ) -> Result<(), R1CSError> {
        let mut res = Variable::One();
        let mut res_witness = Scalar::one();
        // Sliding window over text with size equal to word length
        for i in 0..(msg.len() - bad_word.len() + 1) {
            let mut acc = LinearCombination::from(Scalar::zero());
            let mut acc_witness = Scalar::zero();
            for j in 0..bad_word.len() {
                // Get characters
                let c1 = msg[i + j];
                let c2 = bad_word[j];
                // Compare characters
                let eq = c1 - c2;
                // Apply AND over each character
                // WARNING: This is not secure, addition should be randomized
                acc = acc + eq;

                // Do the same for the witness
                if let Some(v) = msg_witness {
                    acc_witness = acc_witness + Scalar::from(v[i + j]) - Scalar::from(c2)
                }
            }
            // Apply OR over each window
            res = cs.multiply(LinearCombination::from(res), acc).2;
            res_witness *= acc_witness;
        }
        // Negate the result
        let c = is_zero(cs, res, msg_witness.map(|_| res_witness))?;
        // Constrain
        cs.constrain(c);
        Ok(())
    }

    /// Allocate variables based on the witness message for the prover
    ///
    /// # Arguments
    ///
    /// * cs: Prover
    /// * msg: Byte slice containing the message
    fn allocate_text_prover(
        cs: &mut Prover<&mut Transcript>,
        msg: &[u8],
    ) -> Result<Vec<Variable>, R1CSError> {
        msg.iter()
            .map(|c| cs.allocate(Some(Scalar::from(*c))))
            .collect()
    }

    /// Allocate variables based on the witness message length for the verifier
    ///
    /// # Arguments
    ///
    /// * cs: Verifier
    /// * len: Length of the original message (the number of variables to allocate)
    fn allocate_text_verifier(
        cs: &mut Verifier<&mut Transcript>,
        len: usize,
    ) -> Result<Vec<Variable>, R1CSError> {
        (0..len).map(|_| cs.allocate(None)).collect()
    }

    /// Prove that the message does not contain the bad word
    ///
    /// # Arguments
    ///
    /// * pc_gens: Pedersen generators
    /// * bp_gens: Bulletproof generators
    /// * transcript: Proof transcript
    /// * msg: Bytevector containing the *plaintext* message
    /// * bad_word: Censored word as u8 slice
    pub fn prove(
        pc_gens: &PedersenGens,
        bp_gens: &BulletproofGens,
        transcript: &mut Transcript,
        msg: &[u8],
        bad_word: &[u8],
    ) -> Result<Self, R1CSError> {
        let mut prover = Prover::new(pc_gens, transcript);
        let vars = Self::allocate_text_prover(&mut prover, msg)?;
        Self::gadget(&mut prover, vars, Some(msg), bad_word)?;
        let proof = prover.prove(bp_gens)?;
        Ok(CensorProof(proof))
    }

    /// Verify the proof
    ///
    /// # Arguments
    ///
    /// * pc_gens: Pedersen generators
    /// * bp_gens: Bulletproof generators
    /// * transcript: Proof transcript
    /// * len: Length of the original message
    /// * bad_word: Censored word as u8 slice
    pub fn verify(
        &self,
        pc_gens: &PedersenGens,
        bp_gens: &BulletproofGens,
        transcript: &mut Transcript,
        len: usize,
        bad_word: &[u8],
    ) -> Result<(), R1CSError> {
        let CensorProof(proof) = self;
        let mut verifier = Verifier::new(transcript);
        let vars = Self::allocate_text_verifier(&mut verifier, len)?;
        Self::gadget(&mut verifier, vars, None, bad_word)?;
        verifier.verify(proof, pc_gens, bp_gens)?;
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const PT_1: &[u8; 25] = b"crypto means cryptography";

    #[test]
    fn test_in() {
        // Proof setup variables
        let pc_gens = PedersenGens::default();
        let bp_gens = BulletproofGens::new(1024, 1);
        let mut prover_transcript = Transcript::new(b"is_in");
        let target = b"graph";
        // Generate proof
        let proof =
            CensorProof::prove(&pc_gens, &bp_gens, &mut prover_transcript, PT_1, target).unwrap();

        // Verification transcript
        let mut verifier_transcript = Transcript::new(b"is_in");
        // Verify proof
        let res = proof.verify(
            &pc_gens,
            &bp_gens,
            &mut verifier_transcript,
            PT_1.len(),
            target,
        );
        assert!(!res.is_ok());
    }

    #[test]
    fn test_nin() {
        // Proof setup variables
        let pc_gens = PedersenGens::default();
        let bp_gens = BulletproofGens::new(1024, 1);
        let mut prover_transcript = Transcript::new(b"is_nin");
        let target = b"curren";
        // Generate proof
        let proof =
            CensorProof::prove(&pc_gens, &bp_gens, &mut prover_transcript, PT_1, target).unwrap();

        // Verification transcript
        let mut verifier_transcript = Transcript::new(b"is_nin");
        // Verify proof
        let res = proof.verify(
            &pc_gens,
            &bp_gens,
            &mut verifier_transcript,
            PT_1.len(),
            target,
        );
        assert!(res.is_ok());
    }
}
