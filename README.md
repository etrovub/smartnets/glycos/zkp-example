# ZKP Example
Example of a small zero-knowledge gadget. The prover proofs that a hidden text does not contain a banned word.

## Current implementations:
- Bulletproofs
- Arkworks: trusted setup (Groth16)
- Arkworks: universal setup (Marlin)

## Trusted setup
Groth16 and Marlin both require a setup phase, where Bulletproofs does not.
This leads to a different order of operations.
Effectively, for a setup, the verifier needs to create the gadget first.
For a transparent proof system this order does not matter.

In our example, the verifier requires some information, namely the length of the sentence.
Because of the setup phase in Groth16 and Marlin, the prover needs to preprocess this information and
hand it to the verifier, before creating its own proof.

In the Bulletproofs example, we can simply calculate this information while proving
and hand it, together with the proof, to the verifier.

